# App dedicated for touchable screens for the companies that want to show their offer

The universal application for brands and products presentation on interactive kiosks (1920x1080).
It reads from the config.json file, delivered by a client, information how the customized application is going to look. Then it downloads predefined files from the uploaded folder and creates sections and elements, such:
- background
- font
- logo
- "About company" information
- contact
- products information (description, photo gallery, video player, pdf reader)

The application also includes a contact form section that saves data to the .csv file. 

## Tech Stack

**Client:** Vanilla JS , CSS3, HTML5, VideoJS

**Server:** Node, Electron

## Demo

https://vimeo.com/775738424
https://vimeo.com/775733953

