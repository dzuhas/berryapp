



// ----------- Config -------------------

import { setContentText, setWelcomeText } from "./source/view-handler.js";
import { createListPdf, setListListeners, createKontaktList, createProductList, setListListenersPdfProduct, createVideosList } from "./source/list-handler.js";
import { fullProductOpen, videoOpen } from "./source/element-builder.js";
export var logo

(async () => {
  try {
    const data = await fetch("config.json").then((res) => res.json());

    const { settings, assets, products } = data;
    console.log(settings.desktop)

    const { homeText } = settings;
    const { mainText } = settings;
    const { pdfs } = assets;
    const { contact } = settings;
    const { images } = products
    const { productPdfs } = products
    const { videos } = assets;
    const { desktop } = settings;
    var { logo } = settings;
    const { font } = settings;
    const { filename } = videos;

    const listOfVideosLength = videos.length



    setFont(font);
    setLogo(logo);
    setDesktop(desktop);
    setContentText(mainText);
    setWelcomeText(homeText);
    createListPdf(pdfs);
    setListListeners();
    createKontaktList(contact);
    createProductList(products);
    fullProductOpen(logo);
    setListListenersPdfProduct();
    createVideosList(videos, logo, listOfVideosLength);
    videoOpen()

    return { logo }
  } catch (error) {
    console.log("apskierror");

    console.log(error);
  }
})();

// ----------- WELCOME PAGE ----------

//------------Background-image------


function setDesktop(desktop) {
  document.body.style.backgroundImage = "url(assets/images/" + desktop + ")";

}
// -----------Font-----------

async function setFont(font) {
  const newFont = new FontFace('myFont', 'url(assets/font/' + font + ')');

  await newFont.load();

  document.fonts.add(newFont);
  document.body.classList.add('fonts-loaded');
}

// -----------Button-----------

const startButton = document.getElementById("buttonStart");

startButton.addEventListener("click", () => {
  const start = document.getElementById("welcomePage")
  const main = document.getElementById("main")

  main.classList.toggle("hidden");
  start.classList.toggle("hidden");

});

// ------------Logo------------

function setLogo(logo) {

  function createLogo(src, targetLogo, classname) {
    const logoDiv = document.getElementById(targetLogo);
    const img = document.createElement("img");
    img.setAttribute("src", src);
    img.className = classname;
    logoDiv.appendChild(img);

    return img;
  }



  createLogo("./assets/logo/" + logo, "logoWelcome")
  createLogo("./assets/logo/" + logo, "logoMain", "logoMain")
  createLogo("./assets/logo/" + logo, "logoPdf", "logoPdf")

}





// ----------- MENU ------------------

import { transitionHiddenElement } from '../node_modules/@cloudfour/transition-hidden-element/src/index.js';

export function repleceMenuHidden(nameDiv, classA, classB) {

  nameDiv.classList.remove(classA);
  const reflow = nameDiv.offsetHeight;
  nameDiv.classList.add(classB);

}

function mainHiddenForAll() {

  repleceMenuHidden(pdf, "pdfMain", "hidden")
  repleceMenuHidden(produkty, "produktyMain", "hidden")
  repleceMenuHidden(filmy, "filmyMain", "hidden",)
  repleceMenuHidden(wspolpraca, "wspolpracaMain", "hidden",)
  repleceMenuHidden(kontakt, "kontaktMain", "hidden",)

}

function makeColor(nameDiv, classA, classB) {

  nameDiv.classList.replace(classA, classB);

}

function GreyForAll() {

  makeColor(pdfMenu, "colorRed", "colorGrey")
  makeColor(produktyMenu, "colorRed", "colorGrey")
  makeColor(filmyMenu, "colorRed", "colorGrey",)
  makeColor(wspolpracaMenu, "colorRed", "colorGrey",)
  makeColor(kontaktMenu, "colorRed", "colorGrey",)

}

export function autoTransitioner(x, y) {

  const menuTransitioner = transitionHiddenElement({
    element: document.querySelector(x),
    visibleClass: y,
    waitMode: 'transitionend',
    timeoutDuration: 1000

  });
  menuTransitioner.toggle()
}


const pdfMenuButton = document.getElementById("pdfMenu");

pdfMenuButton.addEventListener("click", () => {
  mainHiddenForAll()
  pdf.classList.remove("hidden");
  GreyForAll()
  makeColor(pdfMenu, "colorGrey", "colorRed")
  autoTransitioner('#pdf', 'pdfMain')

});

// --- MENU PRODUKTY -------


const produktyMenuButton = document.getElementById("produktyMenu");

produktyMenuButton.addEventListener("click", () => {

  mainHiddenForAll()
  produkty.classList.remove("hidden");
  GreyForAll()
  makeColor(produktyMenu, "colorGrey", "colorRed")
  autoTransitioner('#produkty', 'produktyMain')

});

const filmyMenuButton = document.getElementById("filmyMenu");

filmyMenuButton.addEventListener("click", () => {
  mainHiddenForAll()
  filmy.classList.remove("hidden");
  GreyForAll()
  makeColor(filmyMenu, "colorGrey", "colorRed")
  autoTransitioner('#filmy', 'filmyMain')

});

const wspolpracaMenuButton = document.getElementById("wspolpracaMenu");

wspolpracaMenuButton.addEventListener("click", () => {

  mainHiddenForAll()
  wspolpraca.classList.remove("hidden");

  GreyForAll()
  makeColor(wspolpracaMenu, "colorGrey", "colorRed")
  autoTransitioner('#wspolpraca', 'wspolpracaMain')
});



const kontaktMenuButton = document.getElementById("kontaktMenu");

kontaktMenuButton.addEventListener("click", () => {

  mainHiddenForAll()
  kontakt.classList.remove("hidden");
  GreyForAll()
  makeColor(kontaktMenu, "colorGrey", "colorRed")
  autoTransitioner('#kontakt', 'kontaktMain')


});
